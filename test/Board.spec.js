import { equal } from "assert";
import { Board } from "../src/js/Board";


describe('CREATE BOARD', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;


  it('Create each box in board', () => {
    Tablero.generateBoard();
    equal(Tablero.board.length, 140);
  });


  it('Assigns a colour each box', () => {
    Tablero.getColor();
    equal(Tablero.board[74].color, "red");
  });


  /*it('Print each coin on the screen', () => {
    Tablero.printBoard();
    equal();
  });*/

});


describe('INCORRECT MOVE, WHEN NEW BOX IS BUSSY', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;
  const dice = parseInt(6*Math.random()+1)
  
  Tablero.generateBoard();

  it('Player 1 when new box is bussy', () => {                                        //

    Tablero.board[3].player = 1;
    Tablero.board[3].coinNumber[0] = 1;
    Tablero.board[6].player = 2;
    Tablero.board[6].coinNumber[0] = 1;
    Tablero.board[7].player = 3;
    Tablero.board[7].coinNumber[0] = 1;
    Tablero.move(2,1,1);
    Tablero.move(1,1,4);

    equal(Tablero.board[3].player, 1);
    equal(Tablero.board[3].coinNumber[0], 1);
    equal(Tablero.board[3].coinNumber[1], 0);
    equal(Tablero.board[7].player[0],3);
    equal(Tablero.board[7].player[1],2);
    equal(Tablero.board[7].coinNumber[0], 1);
    equal(Tablero.board[7].coinNumber[1], 1);
    
  });


  it('Player 2 when new box is bussy', () => {

    Tablero.board[3].player = 2;
    Tablero.board[3].coinNumber[0] = 2;
    Tablero.board[6].player = 1;
    Tablero.board[6].coinNumber[0] = 2;
    Tablero.board[7].player = 3;
    Tablero.board[7].coinNumber[0] = 2;
    Tablero.move(1,2,1);
    Tablero.move(2,2,4);

    equal(Tablero.board[3].player, 2);
    equal(Tablero.board[3].coinNumber[0], 2);
    equal(Tablero.board[3].coinNumber[1], 0);
    equal(Tablero.board[7].player[0],3);
    equal(Tablero.board[7].player[1],1);
    equal(Tablero.board[7].coinNumber[0], 2);
    equal(Tablero.board[7].coinNumber[1], 2);
   
  });


  it('Player 3 when new box is bussy', () => {

    Tablero.board[3].player = 3;
    Tablero.board[3].coinNumber[0] = 3;
    Tablero.board[6].player = 1;
    Tablero.board[6].coinNumber[0] = 3;
    Tablero.board[7].player = 2;
    Tablero.board[7].coinNumber[0] = 3;
    Tablero.move(1,3,1);
    Tablero.move(3,3,4);

    equal(Tablero.board[3].player, 3);
    equal(Tablero.board[3].coinNumber[0], 3);
    equal(Tablero.board[3].coinNumber[1], 0);
    equal(Tablero.board[7].player[0],2);
    equal(Tablero.board[7].player[1],1);
    equal(Tablero.board[7].coinNumber[0], 3);
    equal(Tablero.board[7].coinNumber[1], 3);
  });


  it('Player 4 when new box is bussy', () => {

    Tablero.board[3].player = 4;
    Tablero.board[3].coinNumber[0] = 4;
    Tablero.board[6].player = 1;
    Tablero.board[6].coinNumber[0] = 4;
    Tablero.board[7].player = 2;
    Tablero.board[7].coinNumber[0] = 4;
    Tablero.move(1,4,1);
    Tablero.move(4,4,4);

    equal(Tablero.board[3].player, 4);
    equal(Tablero.board[3].coinNumber[0], 4);
    equal(Tablero.board[3].coinNumber[1], 0);
    equal(Tablero.board[7].player[0],2);
    equal(Tablero.board[7].player[1],1);
    equal(Tablero.board[7].coinNumber[0], 4);
    equal(Tablero.board[7].coinNumber[1], 4);
    
  });

});


describe('INCORRECT MOVE, BARRIER ON THE WAY!', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;
  const dice = parseInt(6*Math.random()+1)
  
  Tablero.generateBoard();

  it('Player 1 can´t move by barrier', () => {

    Tablero.board[0].player = 1;
    Tablero.board[0].coinNumber[0] = 1;
    Tablero.board[2].player = 2;
    Tablero.board[2].coinNumber[0] = 1;
    Tablero.board[2].coinNumber[1] = 2;
    Tablero.board[2].barrier = true;
    Tablero.move(1,1,dice+2);

    equal(Tablero.board[0].player, 1);
    equal(Tablero.board[0].coinNumber[0], 1);
    equal(Tablero.board[0].coinNumber[1], 0);
    equal(Tablero.board[2].player, 2);
    equal(Tablero.board[2].coinNumber[0], 1);
    equal(Tablero.board[2].coinNumber[1], 2);
    equal(Tablero.board[2].barrier, true);
    
  });


  it('Player 2 can´t move by barrier', () => {

    Tablero.board[0].player = 2;
    Tablero.board[0].coinNumber[0] = 3;
    Tablero.board[2].player = 1;
    Tablero.board[2].coinNumber[0] = 3;
    Tablero.board[2].coinNumber[1] = 4;
    Tablero.move(2,3,dice+2);

    equal(Tablero.board[0].player, 2);
    equal(Tablero.board[0].coinNumber[0], 3);
    equal(Tablero.board[0].coinNumber[1], 0);
    equal(Tablero.board[2].player, 1);
    equal(Tablero.board[2].coinNumber[0], 3);
    equal(Tablero.board[2].coinNumber[1], 4);
    equal(Tablero.board[2].barrier, true);
   
  });


  it('Player 3 can´t move by barrier', () => {

    Tablero.board[0].player = 3;
    Tablero.board[0].coinNumber[0] = 1;
    Tablero.board[2].player = 4;
    Tablero.board[2].coinNumber[0] = 1;
    Tablero.board[2].coinNumber[1] = 2;
    Tablero.move(3,1,dice+2);

    equal(Tablero.board[0].player, 3);
    equal(Tablero.board[0].coinNumber[0], 1);
    equal(Tablero.board[0].coinNumber[1], 0);
    equal(Tablero.board[2].player, 4);
    equal(Tablero.board[2].coinNumber[0], 1);
    equal(Tablero.board[2].coinNumber[1], 2);
    equal(Tablero.board[2].barrier, true);
    
  });


  it('Player 4 can´t move by barrier', () => {

    Tablero.board[0].player = 4;
    Tablero.board[0].coinNumber[0] = 3;
    Tablero.board[2].player = 3;
    Tablero.board[2].coinNumber[0] = 3;
    Tablero.board[2].coinNumber[1] = 4;
    Tablero.move(4,3,dice+2);

    equal(Tablero.board[0].player, 4);
    equal(Tablero.board[0].coinNumber[0], 3);
    equal(Tablero.board[0].coinNumber[1], 0);
    equal(Tablero.board[2].player, 3);
    equal(Tablero.board[2].coinNumber[0], 3);
    equal(Tablero.board[2].coinNumber[1], 4);
    equal(Tablero.board[2].barrier, true);
  });

});


describe('NORMAL TROW, WHEN NEW BOX IS EMPTY', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;
  const dice = parseInt(6*Math.random()+1)
  
  Tablero.generateBoard();

  it('Player 1 normal trow', () => {

    Tablero.board[0].player = 1;
    Tablero.board[0].coinNumber[0] = 1;
    Tablero.move(1,1,dice);

    equal(Tablero.board[0 + dice].player, 1);
    equal(Tablero.board[0 + dice].coinNumber[0], 1);
    equal(Tablero.board[0 + dice].coinNumber[1], 0);
    
  });


  it('Player 2 normal trow', () => {

    Tablero.board[17].player = 2;
    Tablero.board[17].coinNumber[0] = 1;
    Tablero.move(2,1,dice);

    equal(Tablero.board[17 + dice].player, 2);
    equal(Tablero.board[17 + dice].coinNumber[0], 1);
    equal(Tablero.board[17 + dice].coinNumber[1], 0);
  });


  it('Player 3 normal trow', () => {

    Tablero.board[34].player = 3;
    Tablero.board[34].coinNumber[0] = 1;
    Tablero.move(3,1,dice);

    equal(Tablero.board[34 + dice].player, 3);
    equal(Tablero.board[34 + dice].coinNumber[0], 1);
    equal(Tablero.board[34 + dice].coinNumber[1], 0);
  });


  it('Player 4 normal trow', () => {

    Tablero.board[51].player = 4;
    Tablero.board[51].coinNumber[0] = 1;
    Tablero.move(4,1,dice);

    equal(Tablero.board[51 + dice].player, 4);
    equal(Tablero.board[51 + dice].coinNumber[0], 1);
    equal(Tablero.board[51 + dice].coinNumber[1], 0);
  });

});


describe('NEW BOX ARE OCCUPED BY OWN COIN, MAKE BARRIER', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;
  const dice = parseInt(6*Math.random()+1)
  
  Tablero.generateBoard();

  it('Player 1 whit own coin and make barrier', () => {

    Tablero.board[0].player = 1;
    Tablero.board[0].coinNumber[0] = 1;
    Tablero.board[0 + dice].player = 1;
    Tablero.board[0 + dice].coinNumber[0] = 2;
    Tablero.move(1,1,dice);

    equal(Tablero.board[0 + dice].player, 1);
    equal(Tablero.board[0 + dice].coinNumber[0], 2);
    equal(Tablero.board[0 + dice].coinNumber[1], 1);
    equal(Tablero.board[0 + dice].barrier, true);
    
    
  });


  it('Player 2 whit own coin and make barrier', () => {

    Tablero.board[17].player = 2;
    Tablero.board[17].coinNumber[0] = 1;
    Tablero.board[17 + dice].player = 2;
    Tablero.board[17 + dice].coinNumber[0] = 2;
    Tablero.move(2,1,dice);

    equal(Tablero.board[17 + dice].player, 2);
    equal(Tablero.board[17 + dice].coinNumber[0], 2);
    equal(Tablero.board[17 + dice].coinNumber[1], 1);
    equal(Tablero.board[17 + dice].barrier, true);
    
  });


  it('Player 3 whit own coin and make barrier', () => {

    Tablero.board[34].player = 3;
    Tablero.board[34].coinNumber[0] = 1;
    Tablero.board[34 + dice].player = 3;
    Tablero.board[34 + dice].coinNumber[0] = 2;
    Tablero.move(3,1,dice);

    equal(Tablero.board[34 + dice].player, 3);
    equal(Tablero.board[34 + dice].coinNumber[0], 2);
    equal(Tablero.board[34 + dice].coinNumber[1], 1);
    equal(Tablero.board[34 + dice].barrier, true);
  
  });


  it('Player 4 whit own coin and make barrier', () => {

    Tablero.board[51].player = 4;
    Tablero.board[51].coinNumber[0] = 1;
    Tablero.board[51 + dice].player = 4;
    Tablero.board[51 + dice].coinNumber[0] = 2;
    Tablero.move(4,1,dice);

    equal(Tablero.board[51 + dice].player, 4);
    equal(Tablero.board[51 + dice].coinNumber[0], 2);
    equal(Tablero.board[51 + dice].coinNumber[1], 1);
    equal(Tablero.board[51 + dice].barrier, true);
    
  });

});


describe('NEW BOX ARE OCCUPED BY A COIN OF OTHER PLAYER, EAT COIN!', () => {                      // CREATE CORRECTLY EACH BOX TYPE
  
  const Tablero = new Board;
  const dice = parseInt(6*Math.random()+1)
  
  Tablero.generateBoard();

  it('Player 1 new box is occuped by a coin of other player, eat coin!', () => {

    Tablero.board[0].player = 1;
    Tablero.board[0].coinNumber[0] = 1;
    Tablero.board[0 + dice].player = 2;
    Tablero.board[0 + dice].coinNumber[0] = 1;
    Tablero.move(1,1,dice);

    equal(Tablero.board[0 + dice + 20].player, 1);
    equal(Tablero.board[0 + dice + 20].coinNumber[0], 1);
    
    Tablero.board[0 + dice + 20].player = 0;
    Tablero.board[0 + dice + 20].coinNumber[0] = 0;
  });


  it('Player 2 new box is occuped by a coin of other player, eat coin!', () => {

    Tablero.board[17].player = 2;
    Tablero.board[17].coinNumber[0] = 2;
    Tablero.board[17 + dice].player = 3;
    Tablero.board[17 + dice].coinNumber[0] = 1;
    Tablero.move(2,2,dice);

    equal(Tablero.board[17 + dice + 20].player, 2);
    equal(Tablero.board[17 + dice + 20].coinNumber[0], 2);

    Tablero.board[17 + dice + 20].player = 0;
    Tablero.board[17 + dice + 20].coinNumber[0] = 0;
    
  });


  it('Player 3 new box is occuped by a coin of other player, eat coin!', () => {

    Tablero.board[34].player = 3;
    Tablero.board[34].coinNumber[0] = 2;
    Tablero.board[34 + dice].player = 4;
    Tablero.board[34 + dice].coinNumber[0] = 1;
    Tablero.move(3,2,dice);

    equal(Tablero.board[34 + dice + 20].player, 3);
    equal(Tablero.board[34 + dice + 20].coinNumber[0], 2);

    Tablero.board[34 + dice + 20].player = 0;
    Tablero.board[34 + dice + 20].coinNumber[0] = 0;
    
  
  });


  it('Player 4 new box is occuped by a coin of other player, eat coin!', () => {

    Tablero.board[51].player = 4;
    Tablero.board[51].coinNumber[0] = 2;
    Tablero.board[51 + dice].player = 1;
    Tablero.board[51 + dice].coinNumber[0] = 2;
    Tablero.move(4,2,dice);
 
    equal(Tablero.board[3 + dice].player, 4);
    equal(Tablero.board[3 + dice].coinNumber[0], 2);

    Tablero.board[3 + dice].player = 0;
    Tablero.board[3 + dice].coinNumber[0] = 0;
   
    
  });

});


/*

describe('NORMAL TROW, WHEN NEW BOX IS EMPTY', () => {

    const Tablero = new Board;
    const Parchis = new App;
    const random = Math.floor(Math.random() * (5 - 2)) + 2;
    const dice = parseInt(6*Math.random()+1)

    
  //PLAYER 1
    
    it('Player 1 goes to his road when is between 65/68', () => {
      Tablero.board[63].coinNumber = [1,0]
      Tablero.board[63].player = 1
      Tablero.move(1,1,dice);
      equal(Tablero.board[67 + dice].player, 1);
      equal(Tablero.board[67 + dice].coinNumber[0], 1);
      equal(Tablero.board[67 + dice].coinNumber[1], 0);
    });
  
  
    it('Player 1 if the last trow is bigger ', () => {
      Tablero.board[74].coinNumber = 2
      Tablero.board[74].player = 1
      Tablero.move(1,2,dice);
      equal(Tablero.board[75 - ((74 + dice) % 75)].player, 1);
    });
  
  
    it('Player 1 normal trow', () => {
      Tablero.board[3].coinNumber = 3
      Tablero.board[3].player = 1
      Tablero.move(1,3,dice);
      equal(Tablero.board[3 + dice].player, 1);
    });
    
  
  
  //PLAYER 2
  
  
    it('Player 2 goes to his road when is between 14/17', () => {
      Tablero.board[12].coinNumber = 1
      Tablero.board[12].player = 2
      Tablero.move(2,1,dice);
      equal(Tablero.board[75 + dice].player, 2);
    });
  
  
    it('Player 2 if the last trow is bigger ', () => {
      Tablero.board[82].coinNumber = 2
      Tablero.board[82].player = 2
      Tablero.move(2,2,dice);
      equal(Tablero.board[83 - ((82 + dice) % 83)  ].player, 2);
  
    });
  
  
    it('Player 2 goes to his road when is between 68/0', () => {
      Tablero.board[67].coinNumber = 3
      Tablero.board[67].player = 2
      Tablero.move(2,3,dice);
      equal(Tablero.board[-1 + dice].player, 2);
    });
  
    
    it('Player 2 normal trow', () => {
      Tablero.board[3].coinNumber = 4
      Tablero.board[3].player = 2
      Tablero.move(2,4,dice);
      equal(Tablero.board[3 + dice].player, 2);
    });
  
  
  
  //PLAYER 3
  
  
    it('Player 3 goes to his road when is between 31/34', () => {
      Tablero.board[29].coinNumber = 1
      Tablero.board[29].player = 3
      Tablero.move(3,1,dice);
      equal(Tablero.board[83 + dice].player, 3);
    });
    
  
    it('Player 3 if the last trow is bigger ', () => {
      Tablero.board[90].coinNumber = 2
      Tablero.board[90].player = 3
      Tablero.move(3,2,dice);
      equal(Tablero.board[91 - ((90 + dice) % 91)  ].player, 3);
    });
  
  
    it('Player 3 goes to his road when is between 68/0', () => {
      Tablero.board[67].coinNumber = 3
      Tablero.board[67].player = 3
      Tablero.move(3,3,dice);
      equal(Tablero.board[-1 + dice].player, 3);
    });
  
  
    it('Player 3 normal trow', () => {
      Tablero.board[3].coinNumber = 4
      Tablero.board[3].player = 3
      Tablero.move(3,4,dice);
      equal(Tablero.board[3 + dice].player, 3);
    });
  
  
  
  //PLAYER 4
  
  
    it('Player 4 goes to his road when is between 48/51', () => {
      Tablero.board[46].coinNumber = 1
      Tablero.board[46].player = 4
      Tablero.move(4,1,dice);
      equal(Tablero.board[91 + dice].player, 4);
    });
  
  
    it('Player 4 if the last trow is bigger ', () => {
      Tablero.board[98].coinNumber = 2
      Tablero.board[98].player = 4
      Tablero.move(4,2,dice);
      equal(Tablero.board[99 - ((98 + dice) % 99)  ].player, 4);
    });
  
  
    it('Player 4 goes to his road when is between 68/0', () => {
      Tablero.board[67].coinNumber = 3
      Tablero.board[67].player = 4
      Tablero.move(4,3,dice);
      equal(Tablero.board[-1 + dice].player, 4);
    });
  
  
    it('Player 4 normal trow', () => {
      Tablero.board[3].coinNumber = 4
      Tablero.board[3].player = 4
      Tablero.move(4,4,dice);
      equal(Tablero.board[3 + dice].player, 4);
    });
  
  
  });

  */