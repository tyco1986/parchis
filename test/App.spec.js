import { equal } from "assert";
import { App } from "../src/js/App";
//import { Board } from "../src/js/Board";


describe('APP TEST', () => {
  //const Tablero = new Board;
  const Parchis = new App;
  const random = Math.floor(Math.random() * (5 - 2)) + 2;
  //const dice = parseInt(6*Math.random()+1)


  // APP TEST


  it('Create numbers of players', () => {

    Parchis.getPlayers(random)
    .then(result => equal(Parchis.numberOfPlayers.length, random))
    .catch(error => error)
  });


  it('Create config of players', () => {
    
    if(random === 2){
      Parchis.setGame(random)
      .then(result => {
        equal(Parchis.PlayersArray, [0,1])
        equal(Parchis.PlayersID, [1,3])
        equal(Parchis.playersColors, ["red", "yellow"])
        equal(Parchis.Players[0].ID, 1)
        equal(Parchis.Players[1].ID, 3)
        equal(Parchis.Players[0].color, "red")
        equal(Parchis.Players[1].color, "yellow")
      })
      .catch(error => error)
    }

    if(random === 3){
      Parchis.setGame(random)
      .then(result => {
        equal(Parchis.PlayersArray, [0,1,2])
        equal(Parchis.PlayersID, [1,2,3])
        equal(Parchis.playersColors, ["red","green", "yellow"])
        equal(Parchis.Players[0].ID, 1)
        equal(Parchis.Players[1].ID, 2)
        equal(Parchis.Players[2].ID, 3)
        equal(Parchis.Players[0].color, "red")
        equal(Parchis.Players[1].color, "green")
        equal(Parchis.Players[2].color, "yellow") 
      })
      .catch(error => error)
    }

    if(random === 4){
      Parchis.setGame(random)
      .then(result => {
        equal(Parchis.PlayersArray, [0,1,2,3])
        equal(Parchis.PlayersID, [1,2,3,4])
        equal(Parchis.playersColors, ["red","green", "yellow", "blue"])
        equal(Parchis.Players[0].ID, 1)
        equal(Parchis.Players[1].ID, 2)
        equal(Parchis.Players[2].ID, 3)
        equal(Parchis.Players[3].ID, 4)
        equal(Parchis.Players[0].color, "red")
        equal(Parchis.Players[1].color, "green")
        equal(Parchis.Players[2].color, "yellow")
        equal(Parchis.Players[3].color, "blue")
      })
      .catch(error => error)
    }
    
  });


  it('App trow dice', () => {

    Parchis.trowDice()
    .then(diceResult => {
      equal(diceResult, Number >= 1 || Number <= 6)
      equal(diceResult, Parchis.lastDiceResult)
    })
    .catch(error => error)

  });


  it('Delete specific last coin moved on board', () => {
    Parchis.lastCoinMoved = 2;
    Parchis.Players[0].startHome[1] = 0
    Parchis.BOARD.board[40].player = 1;
    Parchis.BOARD.board[40].coinNumber[0] = 2;

    Parchis.deleteCoin(1)
      equal(Parchis.BOARD.board[40].player, 0)
      equal(Parchis.BOARD.board[40].coinNumber[0], 0)
      equal(Parchis.Players[0].startHome[1], 1)
    
  });


  it('Get out coin of home', () => {

    Parchis.newCoin(2)
      equal(Parchis.BOARD.board[0].player, 1)
      equal(Parchis.BOARD.board[0].coinNumber[1], 2)
      equal(Parchis.BOARD.board[0].coinNumber[0], 1)
      equal(Parchis.BOARD.board[0].barrier, true)
    
  });


  it('Move coin from startBox ', () => {

    Parchis.lastDiceResult = 5;
    Parchis.move(1)
      equal(Parchis.BOARD.board[5].player, 1)
      equal(Parchis.BOARD.board[5].coinNumber[0], 1)
      equal(Parchis.BOARD.board[0].coinNumber[0], 2)
      equal(Parchis.BOARD.board[0].barrier, false)
      
  });

  it('Check who is next player, whitout check boostThrowSix, when dice result is 1 to 5, cheking only diceResult  = 5', () => {

    if(random === 2){
      Parchis.next()
      equal(Parchis.actualPlayer, 1)
      Parchis.next()
      equal(Parchis.actualPlayer, 0)
    }

    if(random === 3){
      Parchis.next()
      equal(Parchis.actualPlayer, 1)
      Parchis.next()
      equal(Parchis.actualPlayer, 2)
      Parchis.next()
      equal(Parchis.actualPlayer, 0)
    }

    if(random === 4){
      Parchis.next()
      equal(Parchis.actualPlayer, 1)
      Parchis.next()
      equal(Parchis.actualPlayer, 2)
      Parchis.next()
      equal(Parchis.actualPlayer, 3)
      Parchis.next()
      equal(Parchis.actualPlayer, 0)
    }

  });


  it('Check who is next player, when diceResult is 6', () => {

    Parchis.lastDiceResult = 6;

    if(random === 2){
      Parchis.whoIsNext(Parchis.lastDiceResult)
      .then(result => {
        equal(Parchis.actualPlayer, 0)
      })
      Parchis.whoIsNext(Parchis.lastDiceResult)
      .then(result => {
        equal(Parchis.actualPlayer, 0)
      })
      Parchis.whoIsNext(Parchis.lastDiceResult)
      .then(result => {
        equal(Parchis.actualPlayer, 1)
      })
      .catch(error => error)
    }


  });


});