export class Board {

    constructor() {
        this.board = [];

    }


    generateBoard() {

        for (var i = 0; i < 140; i++) {

            this.board[i] = {
                number: i + 1,                                   //number of box
                color: this.getColor(i),                         //color of box
                onlyColorBox: this.getOnlyColorBox(i),           //only players color can pass by here
                specialColorBox: this.getSpecialColorBox(i),     //special box, can pass any player, but if is not your color and stop here your coin can go home easy              
                specialGrayBox: this.getSpecialGrayBox(i),       //special gray box, is a secure box, no one can kill your coin here
                player: 0,                                       //number of player occupying the box
                coinNumber: [0, 0],                               //player coin number, one to four
                barrier: false                                   //if two coin of the same player are true     
            };
        }
        this.board[75].coinNumber = [0,0,0,0]
        this.board[83].coinNumber = [0,0,0,0]
        this.board[91].coinNumber = [0,0,0,0]
        this.board[99].coinNumber = [0,0,0,0]

        return this.board

    }

    createPlayers(Players) {

        if (Players === 2) {
            this.board[0].player = 1
            this.board[0].coinNumber[0] = 1
            this.board[34].player = 3
            this.board[34].coinNumber[0] = 1
        }
        else {
            if (Players == 3) {
                this.board[0].player = 1
                this.board[0].coinNumber[0] = 1
                this.board[17].player = 2
                this.board[17].coinNumber[0] = 1
                this.board[34].player = 3
                this.board[34].coinNumber[0] = 1
            }
            else {
                if (Players == 4) {
                    this.board[0].player = 1
                    this.board[0].coinNumber[0] = 1
                    this.board[17].player = 2
                    this.board[17].coinNumber[0] = 1
                    this.board[34].player = 3
                    this.board[34].coinNumber[0] = 1
                    this.board[51].player = 4
                    this.board[51].coinNumber[0] = 1

                }
            }
        }
    }


    getColor(num) {                                          //Check whats color is the box                

        if (num == 0 || num >= 68 && num <= 75) {
            return "red"                                      //Box 69 to 76 and 1 are red
        }
        else if (num == 17 || num >= 76 && num <= 83) {
            return "green"                                   //Box 77 to 84 and 18 are green
        }
        else if (num == 34 || num >= 84 && num <= 91) {
            return "yellow"                                  //Box 85 to 92 and 35 are yellow
        }
        else if (num == 51 || num >= 92) {
            return "blue"                                    //Box 93 a 100 and 52 are blue
        } else {
            return "white"                                   //The rest are white   
        }

    }

    getOnlyColorBox(num) {
        if (num >= 68) {
            return true
        } else {
            return false
        }

    }

    getSpecialColorBox(num) {
        if (num == 0 || num == 17 || num == 34 || num == 51) {
            return true
        } else {
            return false
        }

    }

    getSpecialGrayBox(num) {
        if (num == 7 || num == 12 || num == 24 || num == 29 || num == 41 || num == 46 || num == 58 || num == 63) {
            return true
        } else {
            return false
        }

    }

    printBoard() {                                             //For each box, check if are any player and barrier, and add a div whit his properties 
        const view = document.getElementById("view");
        this.board.map((box) => {
            if (box.player >= 1 && box.player <= 4 && box.barrier == true) {
                view.insertAdjacentHTML('beforeend', `<div class='double-coin ${player.color} ${player.number}'></div>`);
            }
            else if (box.player >= 1 && box.player <= 4) {
                view.insertAdjacentHTML('beforeend', `<div class='coin ${player.color} ${player.number}'></div>`);
            }
            else {
                return false
            }
        });

    }

    getFirstEachBox(player) {                                                         //Get the first box to each player and coin for firstMove()
        if (player == 1) {
            return 0
        } else if (player == 2) {
            return 17
        } else if (player == 3) {
            return 34
        } else if (player == 4) {
            return 51
        }

    }

    moveNewCoin(player, selectedCoin) {                                               // The first move of each coin in home

        let newBox = this.getFirstEachBox(player);                                  // Get the number of new box for each player whit getFirstEachBox()
        let thereIsEmptyBox = this.thereIsEmptyBox(newBox);
        let areEqualColorBarrierOnMyBox = this.thereIsEqualColorBarrier(newBox, player);
        let areEqualCoinColorOnMyBox = this.thereIsEqualCoinColorBox(newBox, player);
        let areDifferentCoinColorOnMyBox = this.areDifferentCoinColorOnMyBox(newBox, player);
        let areOtherAndMeOnMyBox = this.areOtherAndMeOnMyBox(newBox, player);
        let areOtherTwoEnemysOnMyBox = this.areOtherTwoEnemysOnMyBox(newBox, player);

        if (areEqualColorBarrierOnMyBox) {
            return console.log('YOU HAVE TWO COINS OWNS ON HOMEBOX! MOVE!')
        } else {
            if (thereIsEmptyBox) {
                this.moveToMyEmptyBox(newBox, player, selectedCoin)
                return this.board[newBox]
            }
            else {
                if (areEqualCoinColorOnMyBox) {
                    this.moveToEqualCoinColorOnMyBox(newBox, selectedCoin)
                    return this.board[newBox]
                }
                else {
                    if (areDifferentCoinColorOnMyBox) {
                        this.moveToDifferentPlayerOnMyBox(newBox, player, selectedCoin)
                        return this.board[newBox]
                    }
                    else {
                        if (areOtherAndMeOnMyBox) {
                            this.eatCoinOnMyBoxAndBarrier(newBox, player, selectedCoin);
                            return this.board[newBox]
                        }
                        else {
                            if (areOtherTwoEnemysOnMyBox) {
                                this.eatLastCoinArraivedToMyHome(newBox, player, selectedCoin);
                                return this.board[newBox]
                            }
                            else {
                                return true
                            }
                        }
                    }
                }
            }
        }
        //this.printBoard()
    }


    searchSelectedCoinBox(player, selectedCoin) {                                       // Search at the board the selected coin for the player in his turn, return ARRAY!!

        var searchCoin = this.board.filter(box => {
            if (box.player === player) {
                if (box.coinNumber.indexOf(selectedCoin) > -1) {
                    return true
                }
            }
            else {
                if (box.player[1] !== 0) {
                    if ((box.player[0] === player) && (box.coinNumber[0] === selectedCoin)) {
                        return true
                    }
                    else {
                        if ((box.player[1] === player) && (box.coinNumber[1] === selectedCoin)) {
                            return true
                        }
                        else {
                            return false
                        }
                    }
                }
            }
        });
        return searchCoin
    }


    getNewBoxByColor(player, selectedCoin, diceResult) {                                 // The result box by player, each player move different, return only NUMBER!! of new array

        let coinMoved = this.searchSelectedCoinBox(player, selectedCoin);
        let boxResult = coinMoved[0].number - 1 + diceResult;


        if (player === 1) {
            if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 64 && boxResult <= 67)) {
                return boxResult + 4
            } else {
                if ((diceResult === 5 || diceResult === 6) && boxResult === 68) {
                    return boxResult + 4
                } else {
                    if (diceResult === 6 && boxResult === 69) {
                        return boxResult + 4
                    }
                    else {
                        if ((diceResult >= 1 && diceResult <= 6) && boxResult > 75) {
                            return boxResult = 75 - (boxResult % 75)
                        }
                        else {
                            if ((diceResult === 10 || diceResult === 20) && (boxResult >= 64 && boxResult <= 71)) {
                                return boxResult + 4
                            }
                            else {
                                if ((diceResult === 10 || diceResult === 20) && (boxResult >= 72 && boxResult <= 75)) {
                                    return boxResult = 75 - ((boxResult + 4) % 75)
                                }
                                else {
                                    if (diceResult === 10 && (boxResult >= 76 && boxResult <= 82)) {
                                        return boxResult = 75 - (boxResult % 75)
                                    }
                                    else {
                                        if (diceResult === 20 && (boxResult >= 76 && boxResult <= 78)) {
                                            return boxResult = 75 - ((boxResult + 4) % 75)
                                        }
                                        else {
                                            if (diceResult === 10 && boxResult >= 83) {
                                                return boxResult = 68
                                            }
                                            else {
                                                if (diceResult === 20 && boxResult >= 79) {
                                                    return boxResult = 68
                                                }
                                                else {
                                                    return boxResult
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            if (player === 2) {
                if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 68 && boxResult <= 73)) {
                    return boxResult - 68
                }
                else {
                    if (diceResult === 10 && (boxResult >= 68 && boxResult <= 78)) {
                        return boxResult - 68
                    }
                    else {
                        if (diceResult === 20 && (boxResult >= 68 && boxResult <= 80)) {
                            return boxResult - 68
                        }
                        else {
                            if (diceResult === 20 && (boxResult >= 81 && boxResult <= 88)) {
                                return boxResult - 5
                            }
                            else {
                                if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 13 && boxResult <= 16)) {
                                    return boxResult + 63
                                } else {
                                    if ((diceResult === 5 || diceResult === 6) && boxResult === 17) {
                                        return boxResult + 63
                                    } else {
                                        if (diceResult === 6 && boxResult === 18) {
                                            return boxResult + 63
                                        }
                                        else {
                                            if ((diceResult >= 1 && diceResult <= 6) && boxResult > 83) {
                                                return boxResult = 83 - (boxResult % 83)
                                            }
                                            else {
                                                if ((diceResult === 10 || diceResult === 20) && (boxResult >= 13 && boxResult <= 20)) {
                                                    return boxResult + 63
                                                }
                                                else {
                                                    if ((diceResult === 10 || diceResult === 20) && (boxResult >= 21 && boxResult <= 26)) {
                                                        return boxResult = 83 - ((boxResult + 63) % 83)
                                                    }
                                                    else {
                                                        if (diceResult === 20 && boxResult <= 32) {
                                                            return boxResult = 76
                                                        }
                                                        else {
                                                            if (diceResult === 10 && (boxResult >= 84 && boxResult <= 90)) {
                                                                return boxResult = 83 - (boxResult % 83)
                                                            }
                                                            else {
                                                                if (diceResult === 20 && (boxResult >= 84 && boxResult <= 86)) {
                                                                    return boxResult = 83 - ((boxResult + 63) % 83)
                                                                }
                                                                else {
                                                                    if (diceResult === 10 && boxResult >= 91) {
                                                                        return boxResult = 76
                                                                    }
                                                                    else {
                                                                        if (diceResult === 20 && (boxResult >= 87 && boxResult <= 103)) {
                                                                            return boxResult = 76
                                                                        }
                                                                        else {
                                                                            return boxResult
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if (player === 3) {
                    if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 68 && boxResult <= 73)) {
                        return boxResult - 68
                    }
                    else {
                        if (diceResult === 10 && (boxResult >= 68 && boxResult <= 78)) {
                            return boxResult - 68
                        }
                        else {
                            if (diceResult === 20 && (boxResult >= 68 && boxResult <= 88)) {
                                return boxResult - 68
                            }
                            else {
                                if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 30 && boxResult <= 33)) {
                                    return boxResult + 54
                                }
                                else {
                                    if ((diceResult === 5 || diceResult === 6) && boxResult === 34) {
                                        return boxResult + 54
                                    }
                                    else {
                                        if (diceResult === 6 && boxResult === 35) {
                                            return boxResult + 54
                                        }
                                        else {
                                            if ((diceResult >= 1 && diceResult <= 6) && boxResult > 91) {
                                                return boxResult = 91 - (boxResult % 91)
                                            }
                                            else {
                                                if ((diceResult === 10 || diceResult === 20) && (boxResult >= 30 && boxResult <= 37)) {
                                                    return boxResult + 54
                                                }
                                                else {
                                                    if ((diceResult === 10 || diceResult === 20) && (boxResult >= 38 && boxResult <= 41)) {
                                                        return boxResult = 91 - ((boxResult + 54) % 91)
                                                    }
                                                    else {
                                                        if (diceResult === 20 && (boxResult >= 30 && boxResult <= 49)) {
                                                            return boxResult = 84
                                                        }
                                                        else {
                                                            if (diceResult === 10 && (boxResult >= 92 && boxResult <= 98)) {
                                                                return boxResult = 91 - (boxResult % 91)
                                                            }
                                                            else {
                                                                if (diceResult === 20 && (boxResult >= 92 && boxResult <= 96)) {
                                                                    return boxResult = 91 - ((boxResult + 54) % 91)
                                                                }
                                                                else {
                                                                    if (diceResult === 10 && (boxResult >= 99 && boxResult <= 110)) {
                                                                        return boxResult = 84
                                                                    }
                                                                    else {
                                                                        if (diceResult === 20 && (boxResult >= 95 && boxResult <= 115)) {
                                                                            return boxResult = 84
                                                                        }
                                                                        else {
                                                                            return boxResult
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    if (player === 4) {
                        if (boxResult >= 68 && boxResult <= 88) {
                            return boxResult - 68
                        }
                        else {
                            if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 68 && boxResult <= 73)) {
                                return boxResult - 68
                            }
                            else {
                                if (diceResult === 10 && (boxResult >= 68 && boxResult <= 78)) {
                                    return boxResult - 68
                                }
                                else {
                                    if (diceResult === 20 && (boxResult >= 68 && boxResult <= 80)) {
                                        return boxResult - 68
                                    }
                                    else {
                                        if ((diceResult >= 1 && diceResult <= 6) && (boxResult >= 47 && boxResult <= 50)) {
                                            return boxResult + 45
                                        } else {
                                            if ((diceResult === 5 || diceResult === 6) && boxResult === 51) {
                                                return boxResult + 45
                                            } else {
                                                if (diceResult === 6 && boxResult === 52) {
                                                    return boxResult + 45
                                                }
                                                else {
                                                    if ((diceResult >= 1 && diceResult <= 6) && boxResult > 99) {
                                                        return boxResult = 99 - (boxResult % 99)
                                                    }
                                                    else {
                                                        if ((diceResult === 10 || diceResult === 20) && (boxResult >= 47 && boxResult <= 54)) {
                                                            return boxResult + 45
                                                        }
                                                        else {
                                                            if ((diceResult === 10 || diceResult === 20) && (boxResult >= 55 && boxResult <= 59)) {
                                                                return boxResult = 99 - ((boxResult + 45) % 99)
                                                            }
                                                            else {
                                                                if (diceResult === 20 && (boxResult >= 47 && boxResult <= 66)) {
                                                                    return boxResult = 92
                                                                }
                                                                else {
                                                                    if (diceResult === 10 && (boxResult >= 100 && boxResult <= 106)) {
                                                                        return boxResult = 99 - (boxResult % 99)
                                                                    }
                                                                    else {
                                                                        if (diceResult === 20 && (boxResult >= 100 && boxResult <= 104)) {
                                                                            return boxResult = 99 - ((boxResult + 45) % 99)
                                                                        }
                                                                        else {
                                                                            if (diceResult === 10 && (boxResult >= 107 && boxResult <= 118)) {
                                                                                return boxResult = 92
                                                                            }
                                                                            else {
                                                                                if (diceResult === 20 && (boxResult >= 103 && boxResult <= 123)) {
                                                                                    return boxResult = 92
                                                                                }
                                                                                else {
                                                                                    return boxResult
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }



    /////////////////////////// PRINCIPAL MOVE FUNCTION


    move(player, selectedCoin, diceResult) {

        const newBox = this.getNewBoxByColor(player, selectedCoin, diceResult);
        const thereIsFinalBox = this.thereIsFinalBox(newBox, player, selectedCoin);
        const thereIsBusyBox = this.thereIsBusyBox(newBox);
        const thereIsBarrierOnWay = this.thereIsBarrierOnWay(player, selectedCoin, diceResult);
        const thereIsSameBox = this.thereIsSameBox(newBox, player, selectedCoin);
        const thereIsEmptyBox = this.thereIsEmptyBox(newBox);
        const thereIsEqualCoinColorBox = this.thereIsEqualCoinColorBox(newBox, player);
        const thereIsDifferentCoinColorOnWhiteBox = this.thereIsDifferentCoinColorOnWhiteBox(newBox, player);
        const thereIsDifferentCoinColorOnSpecialGreyBox = this.thereIsDifferentCoinColorOnSpecialGreyBox(newBox, player);
        const thereIsDifferentCoinColorOnSpecialColorBox = this.thereIsDifferentCoinColorOnSpecialColorBox(newBox, player);

        if (thereIsFinalBox) {
            return this.moveToFinalBox(newBox, player, selectedCoin)
        }
        else {
            if (thereIsBusyBox) {
                //ViewAdaptor.printBoard() INCORRECT MOVEMENT, SHOW MENSAGGE OR SIMPLY DONT MOVE
                //ViewAdaptor.moveAgain()
                return console.log('BUSSY BOX!!');
            } else {
                if (thereIsBarrierOnWay) {
                    //ViewAdaptor.printBoard() INCORRECT MOVEMENT, SHOW MENSAGGE OR SIMPLY DONT MOVE
                    //ViewAdaptor.moveAgain()
                    return console.log('BARRIER ON THE WAY!!');
                }
                else {
                    if (thereIsSameBox) {
                        return this.board[newBox]
                    }
                    else {
                        if (thereIsEmptyBox) {
                            return this.moveToEmptyBox(newBox, player, selectedCoin);                 //console.log('new box are free')
                        }
                        else {
                            if (thereIsEqualCoinColorBox) {
                                return this.moveToEqualCoinColorBox(newBox, player, selectedCoin);
                            }
                            else {
                                if (thereIsDifferentCoinColorOnWhiteBox) {
                                    return this.moveToDifferentCoinColorOnWhiteBox(newBox, player, selectedCoin)
                                }
                                else {
                                    if (thereIsDifferentCoinColorOnSpecialGreyBox) {
                                        return this.moveToDifferentCoinColorSpecialGreyOrColorBox(newBox, player, selectedCoin)
                                    }
                                    else {
                                        if (thereIsDifferentCoinColorOnSpecialColorBox) {
                                            return this.moveToDifferentCoinColorSpecialGreyOrColorBox(newBox, player, selectedCoin)
                                        } else {
                                            return false
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    //////////////////////////// CHEKING WHICH IS THE NEW MOVEMENT

    thereIsFinalBox(newBox, player) {
        if ((player === 1 && newBox === 75) || (player === 2 && newBox === 83) || (player === 3 && newBox === 91) || (player === 4 && newBox === 99)) {
            return true
        }
        else {
            return false
        }
    }



    thereIsBusyBox(newBox) {                                       //Check if the new box is busy by two players(if are busy by two coins of same player is barrier and check in other function)        

        if (this.board[newBox].player[1] === 1 || this.board[newBox].player[1] === 2 || this.board[newBox].player[1] === 3 || this.board[newBox].player[1] === 4) {
            return true
        } else {
            return false
        }

    }


    thereIsBarrierOnWay(player, selectedCoin, diceResult) {                               //Should return if there is any barrier on the way

        var barrierOnWay = false;

        for (var i = 0; i < diceResult; i++) {
            if (this.board[this.getNewBoxByColor(player, selectedCoin, i) + 1].barrier === true) {
                barrierOnWay = true;
            }
        }
        return barrierOnWay

    }


    thereIsSameBox(newBox, player, selectedCoin) {

        if (this.board[newBox].player === player) {
            if (this.board[newBox].coinNumber[0] === selectedCoin || this.board[newBox].coinNumber[1] === selectedCoin)
                return true
        }
        else {
            return false
        }


    }

    thereIsEmptyBox(newBox) {                                       //Check if the new box is empty        

        if (this.board[newBox].player === 0) {
            return true
        }
        else {
            return false
        }

    }


    thereIsEqualCoinColorBox(newBox, player) {                       //Check if new box is occuped by own coin

        if (this.board[newBox].player === player) {
            return true
        }
        else {
            return false
        }

    }

    thereIsEqualColorBarrier(newBox, player) {
        if (this.board[newBox].player === player && this.board[newBox].barrier === true) {
            return true
        }
        else {
            return false
        }
    }


    thereIsDifferentCoinColorOnWhiteBox(newBox, player) {            //Check if a white new box is occuped by coin of other

        if (this.board[newBox].player !== 0 && this.board[newBox].player !== player) {
            if (!this.board[newBox].specialGrayBox && !this.board[newBox].specialColorBox) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }

    }


    thereIsDifferentCoinColorOnSpecialGreyBox(newBox, player) {             //Check if a grey new box is occuped by coin of other



        if (this.board[newBox].player !== 0 && this.board[newBox].player !== player) {
            if (this.board[newBox].specialGrayBox) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }

    }


    thereIsDifferentCoinColorOnSpecialColorBox(newBox, player) {             //Check if a grey new box is occuped by coin of other

        if (this.board[newBox].player !== 0 && this.board[newBox].player !== player) {
            if (this.board[newBox].specialColorBox) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }

    areDifferentCoinColorOnMyBox(newBox, player) {

        if (this.board[newBox].player !== 0 && this.board[newBox].player !== player) {
            if (this.board[newBox].coinNumber[1] === 0) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }

    }

    areOtherAndMeOnMyBox(newBox, player) {

        if (this.board[newBox].player[0] === player) {
            if (this.board[newBox].player[1] !== player || this.board[newBox].player[1] !== 0) {
                return true
            }
        }
        else if (this.board[newBox].player[1] === player) {
            if (this.board[newBox].player[0] !== player || this.board[newBox].player[0] !== 0) {
                return true
            }
        }
        else {
            return false
        }
    }


    areOtherTwoEnemysOnMyBox(newBox, player) {
        if (this.board[newBox].player[0] !== player || this.board[newBox].player[0] !== 0) {
            if (this.board[newBox].player[1] !== player || this.board[newBox].player[1] !== 0) {
                return true
            }
        }
        else {
            return false
        }
    }



    /////////////////////////////////// DIFFERENT TYPES OF MOVEMENT

    moveToFinalBox(newBox, player, selectedCoin) {

        this.board[newBox].player = player
        this.board[newBox].coinNumber[selectedCoin] = 1
        return this.board[newBox]
    }


    moveToEmptyBox(newBox, player, selectedCoin) {                                 //Move to a new empty box

        let eraseCorrectCoinArrayPosition = this.eraseCorrectCoinArrayPosition(player, selectedCoin);

        this.board[newBox].player = player
        this.board[newBox].coinNumber[0] = selectedCoin
        eraseCorrectCoinArrayPosition;
        return this.board[newBox]
    }


    moveToEqualCoinColorBox(newBox, player, selectedCoin) {                        //Move to a new box when is occuped by own coin


        let eraseCorrectCoinArrayPosition = this.eraseCorrectCoinArrayPosition(player, selectedCoin);

        this.board[newBox].coinNumber[1] = selectedCoin;
        eraseCorrectCoinArrayPosition;

        this.makeBarrier(newBox)
        return this.board[newBox];
    }


    makeBarrier(newBox) {                                                            //Make barrier when move a occuped box by own coin, only call by moveToEqualCoinColorBox

        return this.board[newBox].barrier = true;
    }


    moveToDifferentCoinColorOnWhiteBox(newBox, player, selectedCoin) {             //Move to a white new box when is occuped by coin of other = eatedBoost

        let eraseCorrectCoinArrayPosition = this.eraseCorrectCoinArrayPosition(player, selectedCoin);

        this.board[newBox].player = player
        this.board[newBox].coinNumber[0] = selectedCoin
        eraseCorrectCoinArrayPosition;
        return this.eatedBoost(player, selectedCoin) //WHEN CHANGE BOOSTCOIN TO VIEW ADAPTOR ERASE SELECTED COIN OF ALL eatedBoost()!!!!!
    }

    eatedBoost(player, selectedCoin) {                                                                  // player choose a coin to move twenty positions by eated coin
        const boostedCoin = selectedCoin;
        //const boostedCoin = parseInt(prompt('Enter Coin To Boost It Please!!','Selected Coin')); 
        //const boostedCoin = ViewAdaptor.askForBoost()

        return this.move(player, boostedCoin, 20);
    }


    moveToDifferentCoinColorSpecialGreyOrColorBox(newBox, player, selectedCoin) {              //Move to a grey new box when is occuped by coin of other = this.box.player= [player1,player2]

        let eraseCorrectCoinArrayPosition = this.eraseCorrectCoinArrayPosition(player, selectedCoin);
        let remainPlayer = this.board[newBox].player;

        this.board[newBox].player = [remainPlayer, 0];
        this.board[newBox].player[1] = player
        this.board[newBox].coinNumber[1] = selectedCoin
        eraseCorrectCoinArrayPosition;
        return this.board[newBox];
    }

    moveToMyEmptyBox(newBox, player, selectedCoin) {                                 //Move to a new empty box
        
        this.board[newBox].player = player
        this.board[newBox].coinNumber[0] = selectedCoin
        return this.board[newBox]
    }

    moveToEqualCoinColorOnMyBox(newBox, selectedCoin) {

        this.board[newBox].coinNumber[1] = selectedCoin;
        this.makeBarrier(newBox)
        return this.board[newBox];
    }

    moveToDifferentPlayerOnMyBox(newBox, player, selectedCoin) {
        let remaininPlayer = this.board[newBox].player;
        let remainingCoin = this.board[newBox].coinNumber.shift();

        this.board[newBox].player = [player, remaininPlayer]
        this.board[newBox].coinNumber = [selectedCoin, remainingCoin]
        return this.board[newBox];
    }

    eatCoinOnMyBoxAndBarrier(newBox, player, selectedCoin) {

        let remainingCoin = this.board[newBox].coinNumber.shift();

        this.board[newBox].player = player
        this.board[newBox].coinNumber = [remainingCoin, selectedCoin]
        this.makeBarrier(newBox)
        this.eatedBoost(player, selectedCoin)
        return this.board[newBox];
    }

    eatLastCoinArraivedToMyHome(newBox, player, selectedCoin) {

        let remainingPlayer = this.board[newBox].player.pop();
        let remainingCoin = this.board[newBox].coinNumber.pop();

        this.board[newBox].player = [player, remainingPlayer]
        this.board[newBox].coinNumber = [selectedCoin, remainingCoin]
        this.eatedBoost(player, selectedCoin)
        return this.board[newBox];
    }



    ///////////////////////////////////  DELETE THE OLD BOX ACCORDING TO THE STATE OF THE OLD BOX


    eraseCorrectCoinArrayPosition(player, selectedCoin) {

        let oldBox = this.searchSelectedCoinBox(player, selectedCoin);
        const indexOfSelectedCoin = oldBox[0].coinNumber.indexOf(selectedCoin);

        if (oldBox[0].player[1] !== 0) {
            if ((oldBox[0].player[0] === player) && (oldBox[0].coinNumber[0] === selectedCoin)) {
                return this.eraseDoublePlayerOnGreyBoxFirstHole(oldBox)
            }
            else {
                if ((oldBox[0].player[1] === player) && (oldBox[0].coinNumber[1] === selectedCoin)) {
                    return this.eraseDoublePlayerOnGreyBoxSecondHole(oldBox, player)
                }
                else {
                    if (indexOfSelectedCoin === 0) {
                        if (oldBox[0].coinNumber[1] !== 0) {

                            return this.changeArrayCoinPosition(oldBox);
                        }
                        else {
                            return this.eraseAllOldBox(oldBox);
                        }
                    }
                    else if (indexOfSelectedCoin === 1) {
                        return this.eraseOnlyLastPositionCoin(oldBox);
                    }
                    else {
                        return false
                    }
                }
            }
        }
        else {

        }
    }


    eraseAllOldBox(oldBox) {                                                                 // CLEAR OLD BOX COMPLETELY WHEN COIN ARE ALONE IN BOX

        let arrayToErase = oldBox[0].number - 1;

        this.board[arrayToErase].player = 0;
        this.board[arrayToErase].coinNumber = [0, 0]
    }


    changeArrayCoinPosition(oldBox) {                                                        // WHEN COIN MOVED IS ON POSIC 0, REMAIN COIN ON POSIC 1 SHOULD BE MOVE TO POSIC 0

        let arrayToChangePosition = oldBox[0].number - 1;
        let remainingCoin = this.board[arrayToChangePosition].coinNumber.pop();

        this.board[arrayToChangePosition].coinNumber = [remainingCoin, 0];
        return this.unmakeBarrierIfExist(oldBox)
    }


    eraseOnlyLastPositionCoin(oldBox) {

        let arrayToEraseLastPosition = oldBox[0].number - 1;

        this.board[arrayToEraseLastPosition].coinNumber[1] = 0
        return this.unmakeBarrierIfExist(oldBox)
    }


    unmakeBarrierIfExist(oldBox) {                                                           // UNDO BARRIER OF OLD BOX

        let boxToCheckBarrier = oldBox;

        if (boxToCheckBarrier[0].barrier === true) {
            return this.board[boxToCheckBarrier[0].number - 1].barrier = false
        }
        else {
            return false
        }
    }


    eraseDoublePlayerOnGreyBoxFirstHole(oldBox) {

        let doublePlayerOnGreyBoxFirstHole = oldBox[0].number - 1;
        let remainingPlayer = this.board[doublePlayerOnGreyBoxFirstHole].player.pop();
        let remainingCoin = this.board[doublePlayerOnGreyBoxFirstHole].coinNumber.pop();

        this.board[doublePlayerOnGreyBoxFirstHole].player = remainingPlayer
        this.board[doublePlayerOnGreyBoxFirstHole].coinNumber = [remainingCoin, 0]

    }

    eraseDoublePlayerOnGreyBoxSecondHole(oldBox) {

        let doublePlayerOnGreyBoxSecondHole = oldBox[0].number - 1;
        let remainingPlayer = this.board[doublePlayerOnGreyBoxSecondHole].player.shift();

        this.board[doublePlayerOnGreyBoxSecondHole].player = remainingPlayer
        this.board[doublePlayerOnGreyBoxSecondHole].coinNumber[1] = 0
    }
}

/*
if(typeof document !== 'undefined') {
    document.newboard = new Board;                                                                        // My test

    document.newboard.generateBoard();

    document.newboard.board[34].coinNumber[0] = 1
    document.newboard.board[34].player = 3


/*
    document.newboard.board[34].coinNumber[0] = 1
    document.newboard.board[34].player = 3
    document.newboard.board[24].coinNumber = [2,3]
    document.newboard.board[24].player = [3,4]
    document.newboard.board[22].coinNumber[0] = 1
    document.newboard.board[22].player = 4
    document.newboard.board[27].coinNumber[0] = 1
    document.newboard.board[27].player = 1
    document.newboard.board[12].coinNumber[0] = 3
    document.newboard.board[12].barrier = true
    document.newboard.board[12].coinNumber[1] = 4
    document.newboard.board[12].player = 3
    document.newboard.board[15].coinNumber[0] = 1
    document.newboard.board[15].player = 2
    document.newboard.board[16].coinNumber[0] = 3
    document.newboard.board[16].player = 1
    document.newboard.board[14].coinNumber[0] = 2
    document.newboard.board[14].player = 4

    console.log(document.newboard);

    console.log('document.newboard.board[34].coinNumber[0] = 1');
    console.log('document.newboard.board[34].player = 3');
    console.log('document.newboard.board[24].coinNumber = [2,3]');
    console.log('document.newboard.board[24].player = [3,4]');
    console.log('document.newboard.board[22].coinNumber[0] = 1');
    console.log('document.newboard.board[22].player = 4');
    console.log('document.newboard.board[27].coinNumber[0] = 1');
    console.log('document.newboard.board[27].player = 1');
    console.log('document.newboard.board[12].coinNumber[0] = 3');
    console.log('document.newboard.board[12].barrier = true');
    console.log('document.newboard.board[12].coinNumber[1] = 4');
    console.log('document.newboard.board[12].player = 3');
    console.log('document.newboard.board[15].coinNumber[0] = 1');
    console.log('document.newboard.board[15].player = 2');
    console.log('document.newboard.board[16].coinNumber[0] = 3');
    console.log('document.newboard.board[16].player = 1');
    console.log('document.newboard.board[14].coinNumber[0] = 2');
    console.log('document.newboard.board[14].player = 4');

}*/








/// QUE HACE MOVE   (EN CADA MOVIMIENTO DEBERIA COMPROBAR
                        //1º SI HAY BARRERA EN EL CAMINO PARA PODER HACER EL MOVIMIENTO
                        //2º SABER EN QUE POSICION DE coinNumber[] ESTA EL COIN PARA MOVER ESE Y NO OTRO (SE PUEDE FILTRAR SI TIENE BARRERA)
                        //3º SI EL COIN QUE SE MUEVE ES EL 0, EL QUE ESTA EN LA POSICION 1 PASARLO A LA POSICION 0





/// MUEVE LA FICHA DEPENDE DEL JUGADOR 
/// SI NO HAY NADIE, MUEVE LA FICHA A ESA CASILLA
/// SI HAY ALGUIEN EN LA CASILLA de su color, hace barrera
/// SI HAT ALGUIEN DIFERENTE DE SU COLOR, SE COME LA FICHA
    // LA FICHA COMIDA VUELVE A SU CASA
    /// EL JUGADOR QUE HA COMIDO CUENTA 20 CON LA FICHA SUYA QUE ELIJA
/// SI HAY UNA BARRERA EN EL CAMINO, EL MOVIMIENTO NO PUEDE HACERSE