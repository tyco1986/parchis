import '../styles/index.scss';
import { Player } from "./Player";
import { Board } from "./Board";
import { Dice } from "./Dice";
import { ViewController } from "./ViewController" 

export class App {

    constructor() {

        this.numberOfPlayers = 0;
        this.Players = [];
        this.PlayersID = [];
        this.PlayersArray = [];
        this.playersColors = [];
        this.BOARD = new Board;
        this.actualPlayer = 0;
        this.boostThrowSix = 0;
        this.lastDiceResult = 0;
        this.lastCoinMoved = 0;
        this.moveForEachBox = []
        this.winner = 5;
    }


    getPlayers(){

        return new Promise ((resolve, reject) => {
            var playerButton = document.getElementById("player_button");
            playerButton.addEventListener("click", () => {

                var num = parseInt(document.getElementById("number_players").value);
            
                    this.numberOfPlayers = num 
                    document.getElementById('form_players').style.display = 'none'
                    resolve(num)
            });    
        });
    }    
   

    setGame(num){
        
        return new Promise((resolve,reject) => {

            this.BOARD.generateBoard()
            this.BOARD.createPlayers(num)

            if(num === 2){
                this.PlayersArray = [0,1]
                this.PlayersID = [1,3]
                this.playersColors = ["red", "yellow"]
            }
            else{
                if(num === 3){
                    this.PlayersArray = [0,1,2]
                    this.PlayersID = [1,2,3]
                    this.playersColors = ["red", "green", "yellow"]
                }
                else{
                    if(num === 4){
                        this.PlayersArray = [0,1,2,3]
                        this.PlayersID = [1,2,3,4]
                        this.playersColors = ["red", "green", "yellow", "blue"]
                    }
                }
            }

            for (let player = 0; player < num; player++) {

                var newPlayer = new Player;
                this.Players[player] = newPlayer;
                this.Players[player].changeColor(this.playersColors[player]);
                this.Players[player].changeId(this.PlayersID[player]);
            }

            //console.log(this.BOARD) //DELETEEEEEEE
            //console.log(this.Players) //DELETEEEEE
            
            resolve(num)
        })
    }
   

    trowDice(){

        const DICE = new Dice;

        return new Promise((resolve,reject) =>{
            var diceButton = document.getElementById("dice_button");
            
            diceButton.addEventListener("click", () => {
                 var diceResult = DICE.trow();
                 this.lastDiceResult = diceResult;
                resolve(diceResult);
            });
        });
        
    }


    deleteCoin(playerID){
        console.log('Player ',this.actualPlayer+1, 'Dice´s result is',this.lastDiceResult, 'for three times, last coin moved go home')          ///DELETEEEEEEEEEEEEEEE
        this.Players[this.actualPlayer].coinGoHome(this.lastCoinMoved)
        this.BOARD.eraseCorrectCoinArrayPosition(playerID, this.lastCoinMoved)
    }



    selectMove(diceResult){

        return new Promise ((resolve,reject) => {
            
            if(diceResult === 5){
                var selectMoveButton = document.getElementById("select_move_button");
                    selectMoveButton.addEventListener("click", () => {
                        var selectMove = parseInt(document.getElementById("select_move").value);
                        resolve(selectMove)
                    })
            }
            else{
                var selectMove = 2;
                resolve(selectMove)
            }
        })
    }


    newCoin(whatCoinOutHome){
        
        this.lastCoinMoved = whatCoinOutHome;
        this.Players[this.actualPlayer].startHome[whatCoinOutHome-1] = 0
        this.BOARD.moveNewCoin(this.PlayersID[this.actualPlayer],whatCoinOutHome)
        console.log('Out coin ',this.lastCoinMoved)
    }


    move(whatCoinMove){
        
        this.lastCoinMoved = whatCoinMove;
        this.getMoveForEachBox(this.PlayersID[this.actualPlayer],whatCoinMove,this.lastDiceResult)
        this.BOARD.move(this.PlayersID[this.actualPlayer],whatCoinMove,this.lastDiceResult)
    }


    turn(){

        return new Promise ((resolve,reject) => {

            console.log('Turn player ', this.actualPlayer + 1)       // DELETEEEEE

            var playerID = this.PlayersID[this.actualPlayer]
            
            this.trowDice()
            .then(diceResult => {
                
                if(diceResult === 6 && this.boostThrowSix === 2){
                    this.deleteCoin(playerID)
                    resolve(diceResult)
                }
                else{
                    document.getElementById('form_select_move').style.display = 'inline-block'      //VIEW
                    console.log('Player ',this.actualPlayer+1, 'Dice´s result is',diceResult,)                 ///DELETEEEEEEEEEEEEEEE
                    this.selectMove(diceResult)

                    .then(selectMove => {
                        document.getElementById('form_select_move').style.display = 'none'          //VIEW
                        if(selectMove  === 1){
                            document.getElementById('form_select_coin_out').style.display = 'inline-block'          //VIEW
                            this.Players[this.actualPlayer].selectCoinOut()
        
                            .then(whatCoinOutHome => {
                                document.getElementById('form_select_coin_out').style.display = 'none'              //VIEW
                                this.newCoin(whatCoinOutHome)
                                resolve(this.lastDiceResult)
                            })
                        }
                        else{
                            document.getElementById('form_select_coin_move').style.display = 'inline-block'         //VIEW
                            this.Players[this.actualPlayer].selectCoinMove()
        
                            .then(whatCoinMove => {
                                document.getElementById('form_select_coin_move').style.display = 'none'             //VIEW
                                this.move(whatCoinMove)
                                resolve(this.lastDiceResult)
                            })    
                        }
                    })         
                }       
            })    
        })
    }


    init(){

        this.getPlayers()
        .then(players => this.setGame(players))
        .then(num => this.start())
        .catch(error => console.log(error))      
    }


    start(){

        if(this.winner === 5){
            this.turn()
        .then(diceResult => this.playerWinner(diceResult))
        .then(diceResult => this.whoIsNext(diceResult))
        .then(result => this.start())
        .catch(error => console.log(error))
        }
        else{
            return this.winnerIs()
        }  
    }

        
    whoIsNext(diceResult){

        return new Promise ((resolve) => {
            if(diceResult === 6){
                if(this.boostThrowSix === 2){
                    this.next()
                    resolve(this.actualPlayer)
                }
                else{
                    this.boostThrowSix++
                    resolve(this.actualPlayer)
                }
            }
            else{
                this.next()
                resolve(this.actualPlayer)
            }
        })
    }


    next(){

        const currentIndex = this.PlayersArray.indexOf(this.actualPlayer);
        const nextIndex = (currentIndex + 1) % this.PlayersArray.length;
            this.actualPlayer = this.PlayersArray[nextIndex];
            this.boostThrowSix = 0;
    }


    playerWinner(diceResult){                                                           // TO CHECK
        return new Promise ((resolve) => {

            if(this.BOARD.board[75].coinNumber === [1,1,1,1] || this.BOARD.board[83].coinNumber === [1,1,1,1] || this.BOARD.board[91].coinNumber === [1,1,1,1] || this.BOARD.board[99].coinNumber === [1,1,1,1]){
                this.winner = this.actualPlayer;
                resolve(diceResult)
            }
            else{
                resolve(diceResult)
            }
        })
    }
    

    winnerIs(){
        return console.log('Winner is ', this.winner)
    }


    getMoveForEachBox(player,selectedCoin,diceResult){

        this.moveForEachBox = [];

        for (var i = 1; i <= diceResult; i++) {
            var newStep = this.BOARD.getNewBoxByColor(player, selectedCoin, i) + 1;
            this.moveForEachBox[i-1] = newStep;
        }
        console.log('Coin ',this.lastCoinMoved, 'it moved',this.moveForEachBox) //DELETEEEE
    }


    areThisPlayerCoinsOnBarrier(player) {//TODO, TOO IN PLAYER
        if (this.Players[player].stop[0] === 0 && this.Players[player].stop[1] === 0) {
            return true
        }
        else {
            return false
        }

    }

}



const PACHISI = new App;
PACHISI.init()



//TODO Player change array this.startHome, when own coin is eated, now when boostThrow is === 2 is change ok
//TODO areThisPlayerBarrier()
//TODO when four coin are in final box thereIsWinner()

//TODO in board when coin is eated, ask to player what coin wanna move
