class ViewController {

    constructor(appId, board) {
        this.element = document.getElementById(appId);
        this.board = this.printBoard(board);
    }

    getBoard() {
        return this.board;
    }
    
    printBoard(board) {
        let htmlBoard = document.createElement('div', BOARD_OPTIONS); 
        board.forEach((e,i) => {
            htmlBoard = htmlBoard.appendChild(this.printCell());
        })
        this.element.innerHtml(htmlBoard);
        return htmlBoard;
    }
    
    printCell(cell) {
        let htmlCell = document.createElement('div', getCellOptions(cell));
        return printPlayers(cell.players,htmlCell);
    }

    getCellOptions(cell){
        let cellOptions = {};
        cellOptions.class = 'cell cell--white';
        if(cell.colour === 'red'){
            /// TODO: logic to add cell-colour classname to cellOptions.class
            cellOptions.class = cellOptions.class + 'red';
        }
        return cellOptions;
    }
    
    printPlayers(cellPlayers,htmlCell) {
        /// TODO: cellPlayers.each -> create one span and append to htmlCell per each
        let coins = document.createElement('span', getCoinOptions(cell.player));
        htmlCell.innerHtml(coins);
        return htmlCell;
    }

    getCoinOptions(player) {
        let coinOptions = [{
            'class': 'coin ${coin.player}'
        }]
    }

    moveCoin(coin,newPlace,player) { // maybe player is not needed depending on the board class structure
        this.removeCoin(oldPlace);
        this.board.find(coin)
        this.printPlayers([player],newPlace); // passed as 1 item array so we print using the loop 
    }
    
    removeCoin(oldPlace) {

    }

    createThrowDiceWindow(){
        let throwDiceWindow = document.createElement('div', {'class': 'modal-window modal-window--throwDice'});
    }

    removeAllWindows(){
        delete this.board.getElementsByClassName('window');
    }
}
